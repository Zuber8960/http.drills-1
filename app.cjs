const http = require("http");
const crypto = require('crypto');

const { PORT, HTML_URL, JSON_URL, UUID_URL, STATUS_URL, DELAY_URL } = require('./config');


const server = http.createServer(function (req, res) {
    req.url = req.url.toLowerCase();
    // console.log(req.url);
    if (req.url === HTML_URL) {
        htmlFunction(req, res);
    } else if (req.url === JSON_URL) {
        jsonFunction(req, res);
    } else if (req.url === UUID_URL) {
        uuidFunction(req, res);
    } else if (req.url.includes(STATUS_URL)) {
        statusFunction(req, res);
    } else if (req.url.includes(DELAY_URL)) {
        delayFunction(req, res);
    } else if (req.url === "/") {
        res.statusCode = 200;
        res.setHeader("Content-Type", "text/html");
        res.write(`
        <!DOCTYPE html>
        <html>
            <body  style="background-color: lightcoral; color: #fff">
                <h1> Please click or enter any given endpoint. </h1>
                <hr>
                <ol>
                    <li><a href="${HTML_URL}"> /html </a></li>
                    <li><a href="${JSON_URL}">  /json </a></li>
                    <li><a href=${UUID_URL}>  /uuid </a></li>
                    <li><a href="${STATUS_URL}/200">  /status/(statusCode) </a></li>
                    <li><a href="${DELAY_URL}/1">  /delay/(delayInSeconds) </a></li>
                </ol>
            </body>
        </html>
        `);
        res.end();
    } else {
        res.statusCode = 404;
        res.setHeader("Content-Type", "text/html");
        res.write(`
        <!DOCTYPE html>
        <html>
            <body  style="background-color: lightcoral; color: red">
                <h1> Error 404 : Page Not Found </h1>
                
            </body>
        </html>
        `);
        res.end();
    }
});


function htmlFunction(request, responce) {
    responce.setHeader("Content-Type", "text/html");
    responce.write(`
    <!DOCTYPE html>
    <html>
    <head>
    </head>
    <body>
        <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
        <p> - Martin Fowler</p>

    </body>
    </html>
    `);
    responce.end();
}


function jsonFunction(request, responce) {
    responce.setHeader("Content-Type", "application/json");
    responce.write(`
    {
        "slideshow": {
          "author": "Yours Truly",
          "date": "date of publication",
          "slides": [
            {
              "title": "Wake up to WonderWidgets!",
              "type": "all"
            },
            {
              "items": [
                "Why <em>WonderWidgets</em> are great",
                "Who <em>buys</em> WonderWidgets"
              ],
              "title": "Overview",
              "type": "all"
            }
          ],
          "title": "Sample Slide Show"
        }
      }
    `);
    responce.end();
};


function uuidFunction(request, responce) {
    responce.setHeader("Content-Type", "application/json");
    const cryptoUuid = crypto.randomUUID();
    responce.write(`
    {
        "uuid": ${cryptoUuid}
    }
    `);
    responce.end();
};


function statusFunction(request, responce) {
    responce.setHeader("Content-Type", "application/json");
    let status = +(request.url.split("/")[2]);
    // console.log(status);
    // console.log(http.STATUS_CODES);
    const endpointLength = request.url.split("/").length;
    let message;

    if (http.STATUS_CODES[status] === undefined || endpointLength > 3 || status === 100)  {
        responce.statusCode = 400;
        status = "Invalid";
        message = "Please pass some valid status code";
    }else {
        responce.statusCode = status;
        message = http.STATUS_CODES[status];
    };
    responce.write(`
    {
        "Status Code" : "${status}",
        "Message" : "${message}"
    }
    `);

    responce.end();
};

function delayFunction(request, responce) {
    responce.setHeader("Content-Type", "text/html");
    const time = +(request.url.split("/").pop());
    const endpointLength = request.url.split("/").length;

    if (time < 0 || isNaN(time) === true || endpointLength > 3) {
        responce.statusCode = 400;
        responce.write(`
        <!DOCTYPE html>
        <html>
            <body style="background-color: lightcoral;">
                <h1> Invalid Endpoint !</h1>
                <hr>
                <h1> Please enter some valid endpoint after delay/ </h1>
            </body>
        </html>
        `);
        responce.end();
    } else {
        responce.statusCode = 200;
        setTimeout(() => {
            responce.write(`
            <!DOCTYPE html>
            <html>
                <body style="background-color: lightcoral;">
                    <h1> Sorry for delaying ${time} seconds. </h1>
                </body>
            </html>
            `);
            responce.end();
        }, time * 1000);
    }

};



server.listen(PORT, function () {
    console.log(`Server is now live !`);
})